package com.advancedtaskscheduler;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


public class MapActivity extends Activity {



     // Google Map
    private GoogleMap googleMap;
    private Location myLocation;
    private MarkerOptions myMarkerOptions;
    private Marker myMarker;
    private Button okButton;
    private TextView geoLabel;
    private Intent intent;
    private Intent receivedIntent;
    private String receivedgeoLoc;
    private Button cancelButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        okButton = (Button)findViewById(R.id.mapButtonOk);
        okButton.setEnabled(false);
        cancelButton = (Button) findViewById(R.id.mapButtonCancel);
        geoLabel = (TextView)findViewById(R.id.mapLabel);
        receivedIntent = getIntent();

        receivedgeoLoc = receivedIntent.getStringExtra("geoLoc");

        if (receivedgeoLoc != null && !receivedgeoLoc.isEmpty())
        {
            geoLabel.setText(receivedgeoLoc);
        }



        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
                finish();
            }

        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( geoLabel.getText().toString().trim().equals(""))
                {
                    geoLabel.setError( "Label is required" );
                }
                else
                {
                    if (receivedgeoLoc != null && !receivedgeoLoc.isEmpty())
                    {
                        if (receivedgeoLoc.equals(geoLabel.getText())) {
                            updateGeolocation(receivedgeoLoc, myMarker.getPosition());
                        }
                        else {
                            removeGeolocation(receivedgeoLoc);
                            setGeolocation();
                        }
                        intent = new Intent(MapActivity.this, GeoActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    else {
                        setGeolocation();
                        intent = new Intent(MapActivity.this, GeoActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
            }
        });
    }



    public void setGeolocation()
    {
        ((PowerScheduler) this.getApplication()).setGeolocation(geoLabel.getText().toString(), myMarker.getPosition());
    }

    public LatLng getGeolocation(String name)
    {
      return  ((PowerScheduler) this.getApplication()).getGeolocation(name);
    }
    public void updateGeolocation(String name, LatLng geoLoc)
    {
        ((PowerScheduler) this.getApplication()).setGeolocation(name, geoLoc);
    }
    public void removeGeolocation(String name)
    {
        ((PowerScheduler) this.getApplication()).removeGeolocation(name);
    }
    @Override
    public void onStart()
    {
        super.onStart();
        try {
            // Loading map
            initilizeMap();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setLocationOnMap(){
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
               /* mMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                    public void onSnapshotReady(Bitmap bitmap) {
                        // Write image to disk
                    }
                }); */
                if (receivedgeoLoc != null && !receivedgeoLoc.isEmpty())
                {
                  myMarkerOptions = new MarkerOptions().position(getGeolocation(receivedgeoLoc));
                  myMarker = googleMap.addMarker(myMarkerOptions);
                  CameraPosition cameraPosition = new CameraPosition.Builder().target(getGeolocation(receivedgeoLoc)).zoom(12).build();
                  googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                  okButton.setEnabled(true);
                }
                else {
                    try {
                        myLocation = googleMap.getMyLocation();
                        myMarkerOptions = new MarkerOptions().position(new LatLng(myLocation.getLatitude(), myLocation.getLongitude())).draggable(true);
                        // adding marker
                        myMarker = googleMap.addMarker(myMarkerOptions);

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(
                                new LatLng(myLocation.getLatitude(), myLocation.getLongitude())).zoom(12).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        okButton.setEnabled(true);
                    } catch (Exception e)
                    {
                        e.getStackTrace();
                    }
                }
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {

                if (myMarker != null) {
                    myMarker.remove();
                }
                myMarker = googleMap.addMarker(new MarkerOptions()
                        .position(point).draggable(true));
                okButton.setEnabled(true);
            }
        });
    }

    /**
     * function to load map. If map is not created it will create it for you
     * */
    private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.mapView)).getMap();

            // check if map is created successfully or not
            if (googleMap == null) {

                int checkGooglePlayServices = GooglePlayServicesUtil
                        .isGooglePlayServicesAvailable(this);
                if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
                    // google play services is missing!!!!
              /*
               * Returns status code indicating whether there was an error.
               * Can be one of following in ConnectionResult: SUCCESS,
               * SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
               * SERVICE_DISABLED, SERVICE_INVALID.
               */
                    GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                            this, 1001).show();

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
        setLocationOnMap();
    }

}


