package com.advancedtaskscheduler;

/**
 * Created by liviud on 10/24/2014.
 */
public final class FromExecuteDialogTrigger {
    private static boolean handled = false;

    public static boolean wasHandled(){
        return handled;
    }

    public static void set(){
        handled = true;
    }

    public static void reset(){
        handled = false;
    }
}