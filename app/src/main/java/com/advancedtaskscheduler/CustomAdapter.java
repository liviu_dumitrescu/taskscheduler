package com.advancedtaskscheduler;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<ScheduledJob> jobs;
	private static LayoutInflater inflater = null;
	
	
	public CustomAdapter (Activity act, ArrayList<ScheduledJob> jobs)
	{
		this.activity = act;
		this.jobs = jobs;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	@Override
	public int getCount() {
		return jobs.size();
	}

	@Override
	public Object getItem(int position) {	
		return jobs.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(convertView==null)
            v = inflater.inflate(R.layout.list_row, null);
        TextView title = (TextView)v.findViewById(R.id.title); // title
        TextView period = (TextView)v.findViewById(R.id.period); // period of the job
        TextView timeframe = (TextView)v.findViewById(R.id.timeframe); // timeframe of the job       
        CheckBox jobActive = (CheckBox)v.findViewById(R.id.list_checkbox);//job checkbox
        
        ScheduledJob job = new ScheduledJob();
        job = jobs.get(position);
        
        title.setText(job.getLabel());
        period.setText(job.getJobHour() + ":" + job.getJobMinutes());
        timeframe.setText(job.getStringJobDaysList());
        jobActive.setChecked(job.getEnabled());
        jobActive.setTag(job.id);
        
        jobActive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            		int jobId = (Integer)arg0.getTag();
            	 ((MainActivity) activity).updateEnabledJob(jobId);
            }
        });
        
        
        return v;
	}
	
	public void removeRecord(ScheduledJob object)
	{
		this.jobs.remove(object);
	}
	public void setJobs(ArrayList<ScheduledJob> jobs)
	{
		this.jobs = jobs;
	}

}
