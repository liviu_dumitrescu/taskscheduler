package com.advancedtaskscheduler;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;

import android.content.Intent;
import android.location.Location;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.view.WindowManager;

public class AlarmReceiver extends BroadcastReceiver {
	
	private ScheduledJob receivedJob;
	WifiManager wifiManager; 
	BluetoothAdapter bluetoothAdapter;
	private ArrayList<ScheduledJob> jobs;
	AudioManager am;
    GPSTracker gpsTracker;
    Context mContext;
    PreferenceManager prefManager;
    boolean fromExecuteDialog;


	@Override
	public void onReceive(Context context, Intent intent) {

		receivedJob = (ScheduledJob)intent.getSerializableExtra("myJob");
        fromExecuteDialog = FromExecuteDialogTrigger.wasHandled();


        jobs = readStoredActions();
        for (int i=0;i<jobs.size();i++){
            if (jobs.get(i).id == receivedJob.id)
            {
                receivedJob = jobs.get(i);
                break;
            }
        }

        if (fromExecuteDialog)
        {
            receivedJob.setGeoEnabled(false);
        }

		wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        gpsTracker = new GPSTracker(context);
		mContext = context;
        if (!receivedJob.getGeoEnabled()) {
        executeTask();
    }
        else
    if (receivedJob.getGeoEnabled() && gpsTracker.canGetLocation())
    {
        Location jobLocation = new Location("JobValue");
        Location currentLocation= new Location(gpsTracker.getLocation());
        jobLocation.setLatitude(((PowerScheduler) mContext.getApplicationContext()).getGeolocation(receivedJob.getGeoLocation()).latitude);
        jobLocation.setLongitude(((PowerScheduler) mContext.getApplicationContext()).getGeolocation(receivedJob.getGeoLocation()).longitude);
        float radius=500;

        try
        {
            radius =  Float.parseFloat(prefManager.getDefaultSharedPreferences(mContext).getString("geolimit",""));
        }catch (Exception e){
            e.printStackTrace();
        }

        //set location error to a max of 1,5km


        float distance = jobLocation.distanceTo(currentLocation);
        if (distance < radius)
        {
            executeTask();
        }
        else
        {
            Intent snoozeDialog = new Intent(context, SnoozeDialog.class);
            snoozeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            snoozeDialog.putExtra("myJob", receivedJob);
            snoozeDialog.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            try {
                context.getApplicationContext().startActivity(snoozeDialog);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
        else
    {
        Intent executeDialog = new Intent(context, ExecuteDialog.class);
        executeDialog.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        executeDialog.putExtra("myJob", receivedJob);
        executeDialog.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED + WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        try {
            context.getApplicationContext().startActivity(executeDialog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	}

	public ArrayList<ScheduledJob> readStoredActions()
    {
    	FileInputStream fis;
    	ObjectInputStream is;
    	ArrayList<ScheduledJob> jobs;
		try {
			fis = PowerScheduler.getContext().openFileInput(PowerScheduler.getContext().getString(R.string.objectXMLFile));
			is = new ObjectInputStream(fis);
			jobs = (ArrayList<ScheduledJob>) is.readObject();
			is.close();
		} catch (Exception e) {

			e.printStackTrace();
			jobs = new ArrayList<ScheduledJob>();
		}
    	return jobs;
    }

    public void executeTask()
    {
            if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableWifi)) && !wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(true);

                updateEnabledJob(receivedJob.getId());
                storeActions();

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.disableWifi)) && wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(false);

                updateEnabledJob(receivedJob.getId());
                storeActions();

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableGPRS))) {
                try {
                    final ConnectivityManager conman = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                    final Class<?> conmanClass = Class.forName(conman.getClass().getName());
                    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                    iConnectivityManagerField.setAccessible(true);
                    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                    final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                    final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    setMobileDataEnabledMethod.setAccessible(true);
                    setMobileDataEnabledMethod.invoke(iConnectivityManager, true);

                    updateEnabledJob(receivedJob.getId());
                    storeActions();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.disableGPRS))) {
                try {
                    final ConnectivityManager conman = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                    final Class<?> conmanClass = Class.forName(conman.getClass().getName());
                    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
                    iConnectivityManagerField.setAccessible(true);
                    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
                    final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
                    final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    setMobileDataEnabledMethod.setAccessible(true);
                    setMobileDataEnabledMethod.invoke(iConnectivityManager, false);

                    updateEnabledJob(receivedJob.getId());
                    storeActions();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableVibrate))) {
                am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

                updateEnabledJob(receivedJob.getId());
                storeActions();

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableSilent))) {
                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);

                updateEnabledJob(receivedJob.getId());
                storeActions();

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableNormal))) {
                am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

                updateEnabledJob(receivedJob.getId());
                storeActions();

            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.enableBluetooth))) {
                boolean isEnabled = bluetoothAdapter.isEnabled();
                if (!isEnabled) {
                    bluetoothAdapter.enable();

                    updateEnabledJob(receivedJob.getId());
                    storeActions();

                }
            } else if (receivedJob.getJobActions().equalsIgnoreCase(mContext.getResources().getString(R.string.disableBluetooth))) {
                boolean isEnabled = bluetoothAdapter.isEnabled();
                if (isEnabled) {
                    bluetoothAdapter.disable();

                    updateEnabledJob(receivedJob.getId());
                    storeActions();

                }
            }

        FromExecuteDialogTrigger.reset();
    }


	public void updateEnabledJob(int jobId){
    	jobs = readStoredActions();
    	for (int i=0;i<jobs.size();i++){
	    	if (jobs.get(i).id == jobId)
	    	{	
	    		if (jobs.get(i).getJobDaysList().size() == 0)
		    	jobs.get(i).setEnable(false);
		    	break;
	    	}
    	}
    }
	
	 public void storeActions()
	    {
	    	FileOutputStream fos;
	    	ObjectOutputStream os;
			try {
				fos = PowerScheduler.getContext().openFileOutput(PowerScheduler.getContext().getString(R.string.objectXMLFile), Context.MODE_PRIVATE);
				os = new ObjectOutputStream(fos);
		    	os.writeObject(this.jobs);
		    	os.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }



}
