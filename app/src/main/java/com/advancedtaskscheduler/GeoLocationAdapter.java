package com.advancedtaskscheduler;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.Map;

public class GeoLocationAdapter extends BaseAdapter {

	private Activity activity;
	private ArrayList<String> geoLocations;
	private static LayoutInflater inflater = null;
    private Map<String, LatLng> mapLoc;
    private ArrayList<String> adresses;


	public GeoLocationAdapter(Activity act, ArrayList<String> geoLocations, Map<String, LatLng> mapLoc, ArrayList<String> adresses)
	{
        this.adresses = adresses;
		this.activity = act;
		this.geoLocations = geoLocations;
        this.mapLoc = mapLoc;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	
	@Override
	public int getCount() {
		return geoLocations.size();
	}

	@Override
	public Object getItem(int position) {	
		return geoLocations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
        String address = "";
		if(convertView==null)
            v = inflater.inflate(R.layout.list_row_geolocation, null);

        TextView titleGeolocation = (TextView)v.findViewById(R.id.titleGeolocation); // title
        TextView addressGeolocation = (TextView)v.findViewById(R.id.addressGeolocation); // period of the job


        String geoKey = geoLocations.get(position);
        if (!adresses.isEmpty()) {
            address = adresses.get(position);
        }

        titleGeolocation.setText(geoKey);
        addressGeolocation.setText(address);

        return v;
	}

    public void removeRecord(String geoLocation, int position)
    {
      geoLocations.remove(geoLocation);
      mapLoc.remove(geoLocation);

        if (!adresses.isEmpty()) {
            adresses.remove(position);
        }
    }


}
