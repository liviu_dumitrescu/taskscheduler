package com.advancedtaskscheduler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;


import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.model.LatLng;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GeoActivity extends Activity {

    private Button AddGeoLocationButton;
    private Intent intent;
    private ListView geoList;
    private ArrayList<String> geoString;
    private ArrayList<String> adresses;
    private Map<String, LatLng> mapLoc;
    String selectedGeolocation;
    int selectedPosition;
    private ProgressDialog progressDialog;
    Geocoder geoCoder;
    AdView adView;
    AdRequest adRequest;
    private ArrayList<ScheduledJob> jobs;

  private GeoLocationAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);

        adView = (AdView)this.findViewById(R.id.adViewGeo);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


        geoCoder = new Geocoder(this.getApplicationContext());

        geoString = new ArrayList<String>();
        for ( String key : ((PowerScheduler) this.getApplication()).getGeolocations().keySet() ) {
           geoString.add(key);
        }

        adresses = new ArrayList<String>();
        geoList = (ListView)findViewById(R.id.GeoLocationsList);
        geoList.setVisibility(View.INVISIBLE);
        adapter = new GeoLocationAdapter(this, geoString, ((PowerScheduler) this.getApplication()).getGeolocations(), adresses);
        geoList.setAdapter(adapter);

        AddGeoLocationButton = (Button)findViewById(R.id.AddGeoButton);


        AddGeoLocationButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    AddGeoLocationButton.setBackgroundColor(Color.parseColor("#2C9DC7"));
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    AddGeoLocationButton.setBackgroundColor(Color.TRANSPARENT);
                    AddGeoLocationButton.performClick();
                }
                return true;
            }
        });

        AddGeoLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                intent = new Intent(GeoActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        geoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedGeolocation = (String) adapter.getItem(i);
                intent = new Intent(GeoActivity.this, MapActivity.class);
                intent.putExtra("geoLoc", selectedGeolocation);
                startActivity(intent);
            }
        });
        geoList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                selectedGeolocation = (String) adapter.getItem(position);
                selectedPosition =  position;
                startActionMode(modeCallBack);
                view.setSelected(true);
                return true;

            }
        });

        mapLoc = ((PowerScheduler) this.getApplication()).getGeolocations();
        new LoadAdapter().execute();


    }



    private ActionMode.Callback modeCallBack = new ActionMode.Callback() {

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {


        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Options");
            mode.getMenuInflater().inflate(R.menu.jobsactions, menu);
            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            int id = item.getItemId();
            switch (id) {
                case R.id.delete: {
                    boolean existActiveJob = false;

                    jobs = readStoredActions();
                    for (int i=0;i<jobs.size();i++){
                        if (jobs.get(i).getGeoLocation().equals(selectedGeolocation) && jobs.get(i).getGeoEnabled())
                        {
                            existActiveJob = true;
                            showExistActiveDialog();
                            break;
                        }
                    }

                    if (!existActiveJob) {
                        adapter.removeRecord(selectedGeolocation, selectedPosition);
                        removeGeolocation(selectedGeolocation);
                        storeGeolocations();
                        adapter.notifyDataSetChanged();
                    }

                    mode.finish();
                    break;
                }
                case R.id.edit: {
                    intent = new Intent(GeoActivity.this, MapActivity.class);
                    intent.putExtra("geoLoc", selectedGeolocation);
                    mode.finish();
                    startActivity(intent);
                    break;
                }
                default:
                    return false;

            }
            return true;

        }
    };

    @Override
    public void onStop()
    {
        storeGeolocations();
        super.onStop();
    }


    public void removeGeolocation(String name)
    {
        ((PowerScheduler) this.getApplication()).removeGeolocation(name);
    }

    public void storeGeolocations()
    {
        FileOutputStream fos;
        ObjectOutputStream os;
        try {
            fos = this.openFileOutput(getString(R.string.geoLocationsXMLFile), Context.MODE_PRIVATE);
            os = new ObjectOutputStream(fos);
            os.writeObject(  ((PowerScheduler) this.getApplication()).getSerializableGeolocations() );
            os.close();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private class LoadAdapter extends AsyncTask<Void, Void, Void> {
        private Exception e=null;


        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(GeoActivity.this, "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                for (int i=0; i<geoString.size();i++) {
                    LatLng geoLoc = mapLoc.get(geoString.get(i));
                    List<Address> matches = null;
                    String s="";
                    try {
                        matches = geoCoder.getFromLocation(geoLoc.latitude, geoLoc.longitude, 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                        this.e = e;
                    }
                    Address bestMatch = (matches.isEmpty() ? null : matches.get(0));

                    if (bestMatch != null) {
                       if (bestMatch.getAddressLine(0) != null) {
                           s = s + bestMatch.getAddressLine(0).toString() + " ";
                       }
                       if (bestMatch.getAddressLine(1) != null) {
                           s = s + bestMatch.getAddressLine(1).toString() + " ";
                       }
                       if (bestMatch.getAddressLine(2) != null) {
                           s = s + bestMatch.getAddressLine(2).toString();
                       }
                   }

                    adresses.add(s);
                }
            }catch (Exception e)
            {
              e.printStackTrace();
              this.e = e;
            }
            return  null;
        }
        @Override
        protected void onPostExecute(Void param)
        {
            if (e != null) {
                showDialog();
                geoList.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
            else
            {
                adapter.notifyDataSetChanged();
                geoList.setVisibility(View.VISIBLE);
                progressDialog.dismiss();
            }
        }
    }

    public void showDialog()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Task Scheduler");

        // set dialog message
        alertDialogBuilder
                .setMessage("Could not display address for geolocation, check your internet connection.")
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void showExistActiveDialog()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Task Scheduler");

        // set dialog message
        alertDialogBuilder
                .setMessage("Geolocation is on active task, could not delete!")
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public ArrayList<ScheduledJob> readStoredActions()
    {
        FileInputStream fis;
        ObjectInputStream is;
        ArrayList<ScheduledJob> jobs;
        try {
            fis = PowerScheduler.getContext().openFileInput(PowerScheduler.getContext().getString(R.string.objectXMLFile));
            is = new ObjectInputStream(fis);
            jobs = (ArrayList<ScheduledJob>) is.readObject();
            is.close();
        } catch (Exception e) {

            e.printStackTrace();
            jobs = new ArrayList<ScheduledJob>();
        }
        return jobs;
    }
}
