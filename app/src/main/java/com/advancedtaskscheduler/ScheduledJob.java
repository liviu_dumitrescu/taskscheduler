package com.advancedtaskscheduler;

import java.io.Serializable;
import java.util.ArrayList;

public class ScheduledJob implements Serializable {

	private static final long serialVersionUID = 1L;
	String label;
	int jobHour;
	int jobMinutes;
	ArrayList<String> jobDaysList;
	String jobActions;
    String geoLocation;
	boolean jobEnabled;
    boolean geoEnabled;
	int id;
	
	public ScheduledJob(String label, int jobHour, int jobMinutes, ArrayList<String> jobDaysList, String jobActions, boolean jobEnabled, int id, boolean geoEnabled, String geoLocation)
	{
		this.label = label;
		this.jobHour = jobHour;
		this.jobMinutes = jobMinutes;
		this.jobActions = jobActions;
		this.jobDaysList = jobDaysList;
		this.jobEnabled = jobEnabled;
        this.geoEnabled = geoEnabled;
        this.geoLocation = geoLocation;
		this.id = id;
	}
	
	public ScheduledJob() {
		
	}

	public String getLabel() {
	    return label;
	}
	public void setLabel(String label) {
	    this.label = label;
	}
	public String getJobHour() {
		if (jobHour < 10)
	    return "0"+ Integer.toString(jobHour);
		else
			return Integer.toString(jobHour);
	}
	public void setJobHour(int jobHour) {
	    this.jobHour = jobHour;
	}
	
	public void setId(int id) {
	    this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	public void setJobMinutes(int jobMinutes) {
	    this.jobMinutes = jobMinutes;
	}
	
	public String getJobMinutes() {
		if (jobMinutes < 10)
		    return "0"+ Integer.toString(jobMinutes);
			else
				return Integer.toString(jobMinutes);
	}
	public int getIntJobMinutes()
    {
        return this.jobMinutes;
    }
    public int getIntJobHour()
    {
        return this.jobHour;
    }
	public void setJobDaysList(ArrayList<String> jobDaysList)
	{
		this.jobDaysList = jobDaysList;
	}
	
	public void setJobActions(String jobActions)
	{
		this.jobActions = jobActions;
	}
	
	public String getJobActions (){
		return jobActions;
	}
    public String getGeoLocation (){
        return geoLocation;
    }
    public void setGeoLocation(String geoLocation) {this.geoLocation = geoLocation;}
	
	public ArrayList<String> getJobDaysList (){
		return jobDaysList;
	}

	public String getStringJobDaysList(){
		String s="";
		if (jobDaysList.size() == 7)
		{
			s="Every day";
		}
		else
			if (jobDaysList.isEmpty())
			{
				s="Never";
			}
			else
			{
				for (String str : jobDaysList)
				{
				    s += str.substring(0, 3) + ", ";
				}
				s = s.substring(0, s.length()-2);
			}
		return s;
	}
	
	public void setEnable(boolean value)
	{
		this.jobEnabled = value;
	}
    public  void setGeoEnabled(boolean value) {this.geoEnabled = value;}

	public boolean getEnabled(){
		return jobEnabled;
	}
    public boolean getGeoEnabled(){
        return geoEnabled;
    }
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof ScheduledJob))return false;
	    ScheduledJob otherJob = (ScheduledJob)other;
	    return this.id == otherJob.id;
	   
	}
}
