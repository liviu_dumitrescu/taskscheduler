package com.advancedtaskscheduler;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.WindowManager;
import java.util.Calendar;


public class SnoozeDialog extends Activity {

    private SensorManager mSensorManager;
    private PowerManager mPowerManager;
    private WindowManager mWindowManager;
    private PowerManager.WakeLock mWakeLock;
    private AlarmManager alarmManager;
    private ScheduledJob receivedJob;
    private Intent intent;
    private int snoozeTime;
    PreferenceManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.activity_snooze_dialog);

        snoozeTime = 10;

        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        receivedJob = (ScheduledJob)intent.getSerializableExtra("myJob");
        // Get an instance of the SensorManager
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // Get an instance of the PowerManager
        mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);

        // Get an instance of the WindowManager
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowManager.getDefaultDisplay();

        // Create a bright wake lock
        mWakeLock = mPowerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        mWakeLock.acquire();

        try
        {
            snoozeTime =  Integer.parseInt(prefManager.getDefaultSharedPreferences(this).getString("snooze_frequency",""));
        }catch (Exception e){
            e.printStackTrace();
        }

        showDialog();
        playSound();
        mWakeLock.release();
    }

    public void playSound()
    {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }

    public void showDialog()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Task Scheduler");

        // set dialog message
        alertDialogBuilder
                .setMessage("Could not execute task due to geolocation restriction, please select an action.")
                .setCancelable(false)
                .setPositiveButton("Snooze", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setAlarm(receivedJob);

                        finish();
                    }
                })
                .setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();


                        finish();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void setAlarm(ScheduledJob job)
    {
        Intent intentAlarm;
        intentAlarm = new Intent(this, AlarmReceiver.class);
        intentAlarm.putExtra("myJob", job);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, job.id*999, intentAlarm, 0);


            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + 1000*60*snoozeTime, pendingIntent);
            }
            else
            {
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + 1000*60*snoozeTime, pendingIntent);
            }
        }
    }

