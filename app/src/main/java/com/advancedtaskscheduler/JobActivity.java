package com.advancedtaskscheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

public class JobActivity extends Activity {

	
	private CheckedTextView scheduleJobActive;
    private CheckedTextView scheduleGeoActive;
	private Drawable indicator;
	private TypedArray ta;
	private TextView timeTextView;
	private TextView repeatTextView;
	private TextView labelJob;
	private Calendar mcurrentTime;
	private int hour;
	private int minute;
	private ArrayAdapter<String> listAdapter;
	private ListView listDays;
	private Button dateOkButton;
	private Button dateCancelButton;
	private Button jobOkButton;
	private Button jobCancelButton;
	private Intent receivedIntent;
	private ArrayList<ScheduledJob> jobs;
	private ScheduledJob receivedJob;
	private EditText jobLabel;
	ArrayList<String> selecteddaysList;
	ArrayList<String> tmpselecteddaysList;
	final Context context = this;
	private Spinner jobSpinner;
    private Spinner geoSpinner;
	private int editJob;
    private TextView labelGeoSpinner;
    ArrayAdapter<String> geoadapter;
    ArrayList<String> geoString;
    AdView adView;
    AdRequest adRequest;
    GPSTracker gpsTracker;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_job);


        adView = (AdView)this.findViewById(R.id.adViewJob);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        gpsTracker = new GPSTracker(context);


        int[] attrs = { android.R.attr.listChoiceIndicatorMultiple };
		timeTextView = (TextView)findViewById(R.id.timeTextView);
		labelJob = (TextView)findViewById(R.id.labelSpinner);
        labelGeoSpinner = (TextView) findViewById(R.id.labelgeoSpinner);
		repeatTextView = (TextView)findViewById(R.id.repeatTextView);
		receivedIntent = getIntent();
		jobs = (ArrayList<ScheduledJob>) receivedIntent.getSerializableExtra("myJobs");
		receivedJob = (ScheduledJob)receivedIntent.getSerializableExtra("myJob");
		jobLabel = (EditText)findViewById(R.id.jobLabel);	
		scheduleJobActive = (CheckedTextView)findViewById(R.id.checkedScheduledTextView);
        scheduleGeoActive = (CheckedTextView)findViewById(R.id.checkedGeoTextView);
		editJob = 0;
		jobOkButton  = (Button)findViewById(R.id.jobButtonOk);
		jobCancelButton  = (Button)findViewById(R.id.jobButtonCancel);

        try {
            geoString = new ArrayList<String>();
            for (String key : ((PowerScheduler) this.getApplication()).getGeolocations().keySet()) {
                geoString.add(key);
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }



		jobSpinner = (Spinner) findViewById(R.id.jobSpinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.actions, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		jobSpinner.setAdapter(adapter);

        geoSpinner = (Spinner) findViewById(R.id.geoSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        geoadapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,  geoString);
        // Specify the layout to use when the list of choices appears
        geoadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        geoSpinner.setAdapter(geoadapter);


		
		
		String[] daysOfWeek = new String[] {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
		
		ArrayList<String> daysList = new ArrayList<String>();  
		selecteddaysList = new ArrayList<String>();
		tmpselecteddaysList = new ArrayList<String>();
		daysList.addAll(Arrays.asList(daysOfWeek));
		listAdapter = new ArrayAdapter<String>(this, R.layout.date_items , daysList);		
		labelJob.setText(Html.fromHtml("<b> " + getString(R.string.labelSpinnerText) + "</b>"));
        labelGeoSpinner.setText(Html.fromHtml("<b> " + getString(R.string.labelGeoSpinner) + "</b>"));
		
		ta = getBaseContext().getTheme().obtainStyledAttributes(attrs);
		indicator = ta.getDrawable(0);
		
		timeTextView.setWidth(scheduleJobActive.getWidth());
		repeatTextView.setWidth(scheduleJobActive.getWidth());
		jobLabel.setWidth(scheduleJobActive.getWidth());
		
		mcurrentTime = Calendar.getInstance();
        hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        minute = mcurrentTime.get(Calendar.MINUTE);
		
        if (receivedJob == null)
        {
	        if (minute < 10 && hour > 9)
	    	{
	    		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
	    	            "<small>" + hour+":0"+minute + "</small>"));
	    	}
	    	else if (minute > 9 && hour < 10)
	    	{
	    		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
	    	            "<small>" +"0" + hour+":"+minute + "</small>"));
	    	}
	    	else if (minute < 10 && hour < 10)
	    	{
	    		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
	    	            "<small>" +"0" + hour+":0"+minute + "</small>"));
	    	}
	    	else
	    	{
	    		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
	    	            "<small>" + hour+":"+minute + "</small>"));
	    	}
			repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
		            "<small>" + "Never" + "</small>"));
        }
		
		jobOkButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				ScheduledJob job;
				int id;
				
				if (((PowerScheduler) getApplication()).getSettings().isEmpty())
				{
					id = 0;
				}
				else
					id = ((PowerScheduler) getApplication()).getSettingValue(getString(R.string.maxId));



				if (jobLabel.getText().toString().isEmpty())
				{ if (geoSpinner.getSelectedItem() == null)
                    job = new ScheduledJob(jobSpinner.getSelectedItem().toString(), hour, minute, selecteddaysList, jobSpinner.getSelectedItem().toString(), scheduleJobActive.isChecked(),id, scheduleGeoActive.isChecked(), "none");
                    else
                    job = new ScheduledJob(jobSpinner.getSelectedItem().toString(), hour, minute, selecteddaysList, jobSpinner.getSelectedItem().toString(), scheduleJobActive.isChecked(),id, scheduleGeoActive.isChecked(), geoSpinner.getSelectedItem().toString());}
				else
				{   if (geoSpinner.getSelectedItem() == null)
                    job = new ScheduledJob(jobLabel.getText().toString(), hour, minute, selecteddaysList, jobSpinner.getSelectedItem().toString(), scheduleJobActive.isChecked(),id, scheduleGeoActive.isChecked(), "none");
                    else
                    job = new ScheduledJob(jobLabel.getText().toString(), hour, minute, selecteddaysList, jobSpinner.getSelectedItem().toString(), scheduleJobActive.isChecked(),id, scheduleGeoActive.isChecked(), geoSpinner.getSelectedItem().toString());}
				
				
				
				 Intent nav = new Intent(JobActivity.this, MainActivity.class);
				 if (editJob == 0){				 
					 jobs.add(job);
					 id++;
					 ((PowerScheduler) getApplication()).setSettingValue(getString(R.string.maxId), id);
				 } 
				 else
				 {
					 job.setId(receivedJob.id);
					 jobs.set(jobs.indexOf(receivedJob), job);
				 }
				 
				 
				 if (job.getEnabled())
				 {
				   if (editJob !=0)
				   {
						if (job.getJobDaysList().equals(receivedJob.getJobDaysList()))
						{	
							if (job.getJobDaysList().size() == 0 || job.getJobDaysList().size() == 7){
								 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableAlarm(receivedJob);
								 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setAlarm(job);
							}
								 else
								 {
									 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableDaysAlarm(receivedJob);
									 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setDaysAlarm(job);
								 }
							 
						}
						else if ( (receivedJob.getJobDaysList().size() == 0 && (job.getJobDaysList().size() > 0 && job.getJobDaysList().size() < 7)) || (receivedJob.getJobDaysList().size() == 7 && (job.getJobDaysList().size() > 0 && job.getJobDaysList().size() < 7)))
						{
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableAlarm(receivedJob);
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setDaysAlarm(job);
						}
						else if ((receivedJob.getJobDaysList().size() == 0 && job.getJobDaysList().size()==7) || (receivedJob.getJobDaysList().size() == 7 && job.getJobDaysList().size()==0))
						{
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableAlarm(receivedJob);
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setAlarm(job);
						}
						else
							if ( (receivedJob.getJobDaysList().size() > 0 && receivedJob.getJobDaysList().size() < 7) && (job.getJobDaysList().size() == 0 || job.getJobDaysList().size() == 7) )
							{
								((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableDaysAlarm(receivedJob);
								((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setAlarm(job);
								
							}
							else
						{
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableDaysAlarm(receivedJob);
							((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setDaysAlarm(job);
						}
				   }
				   else
				   {
					   
					   	if (job.getJobDaysList().size() == 0 || job.getJobDaysList().size() == 7)
								 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setAlarm(job);
								 else
								 {
									 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).setDaysAlarm(job);
								 }
				   }
					 
				 }
				 else
				 {
					 if (editJob !=0)
					 {
						 if (receivedJob.getJobDaysList().size() == 0 || receivedJob.getJobDaysList().size() == 7)
						 {
							 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableAlarm(receivedJob);
						 }
						 else
							 ((MainActivity) ((PowerScheduler)getApplication()).getMainActivity()).disableDaysAlarm(receivedJob);
					 }
				 }
				 
				 nav.putExtra("myJobs", jobs);
				 nav.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
				 startActivity(nav);
				
			}
			
		}); 
		
		jobCancelButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
				finish();
			}
			
		});
			
		timeTextView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {

	        	if (editJob == 0)
	        	{
		        	hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
		            minute = mcurrentTime.get(Calendar.MINUTE);
	        	}
	            TimePickerDialog mTimePicker;
	            mTimePicker = new TimePickerDialog(JobActivity.this, new TimePickerDialog.OnTimeSetListener() {
	                @Override
	                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
	                	if (selectedMinute < 10 && selectedHour > 9)
	                	{
	                		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
		            	            "<small>" + selectedHour+":0"+selectedMinute + "</small>"));
	                	}
	                	else if (selectedMinute > 9 && selectedHour < 10)
	                	{
	                		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
		            	            "<small>" +"0" + selectedHour+":"+selectedMinute + "</small>"));
	                	}
	                	else if (selectedMinute < 10 && selectedHour < 10)
	                	{
	                		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
		            	            "<small>" +"0" + selectedHour+":0"+selectedMinute + "</small>"));
	                	}
	                	else
	                	{
	                		timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
		            	            "<small>" + selectedHour+":"+selectedMinute + "</small>"));
	                	}
	                hour = selectedHour;
	                minute = selectedMinute;
	                }
	            }, hour, minute, true);//Yes 24 hour time
	            mTimePicker.setTitle("Select Time");
	            mTimePicker.show();

	        }
	    });
		
		timeTextView.setOnTouchListener(new OnTouchListener() {
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
        	    	timeTextView.setBackgroundColor(Color.parseColor("#2C9DC7"));
        	    }
        	    else if (event.getAction() == MotionEvent.ACTION_UP) {
        	    	timeTextView.setBackgroundResource(R.drawable.lines);
        	    	timeTextView.performClick();
        	    } 
        	    return true;
        	}
        	});

		repeatTextView.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {

	        	final Dialog dialog = new Dialog(context);
				dialog.setTitle("Pick Days");			
				dialog.setContentView(getLayoutInflater().inflate(R.layout.customdate_layout, null));
				listDays = (ListView)dialog.findViewById(R.id.listDays);
				listDays.setAdapter(listAdapter);
				tmpselecteddaysList = new ArrayList<String>(selecteddaysList);
				selecteddaysList.clear();
				listDays.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						int found = 0;
						CheckedTextView tv = (CheckedTextView)view;
						 if (tv.isChecked())
				         {
							 tv.setChecked(false);
							 removeFromList(tv.getText().toString(),selecteddaysList);
				         }
				         else
				         {
				        	 tv.setChecked(true);
				        	 for(String item : selecteddaysList)
				        	    {
				        	        if(item.equalsIgnoreCase(tv.getText().toString()))
				        	        {
				        	            found =1;
				        	        	break;
				        	        }
				        	    }
				        	 if (found == 0)
				        	 selecteddaysList.add(tv.getText().toString());
				         }
						
					}
					         
					  });
				dateOkButton = (Button)dialog.findViewById(R.id.buttonOk);
				dateCancelButton = (Button)dialog.findViewById(R.id.buttonCancel);
				
				dateOkButton.setOnClickListener(new OnClickListener() {
					   
					   @Override
					   public void onClick(View v) {
						String s="";
						
						for (String str : selecteddaysList)
						{
							s += str.substring(0, 3) + ", ";
						}
					    if (selecteddaysList.isEmpty())
					    {
							repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
						            "<small>" + "Never"  + "</small>"));
					    } 
					    else if (selecteddaysList.size() == 7)
					    {
					    	repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
						            "<small>" + "Every day" + "</small>"));
					    }
					    else
					    {
					    repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
					            "<small>" + s.substring(0, s.length()-2)  + "</small>"));
					    }
					    dialog.dismiss();
					   }
					  });
				
				dateCancelButton.setOnClickListener(new OnClickListener() {
					   
					   @Override
					   public void onClick(View v) {
						   String s="";
						   selecteddaysList = new ArrayList<String>(tmpselecteddaysList);
							for (String str : selecteddaysList)
							{
								s += str.substring(0, 3) + ", ";
							}

						    if (selecteddaysList.isEmpty())
						    {
								repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
							            "<small>" + "Never"  + "</small>"));
						    } 
						    else if (selecteddaysList.size() == 7)
						    {
						    	repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
							            "<small>" + "Every day" + "</small>"));
						    }
						    else
						    {
						    repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
						            "<small>" + s.substring(0, s.length()-2)  + "</small>"));
						    }   
						   
					    dialog.dismiss();
					   }
					  });
				
				
				dialog.show();
	        }
	    });
		
		repeatTextView.setOnTouchListener(new OnTouchListener() {
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
        	    	repeatTextView.setBackgroundColor(Color.parseColor("#2C9DC7"));
        	    }
        	    else if (event.getAction() == MotionEvent.ACTION_UP) {
        	    	repeatTextView.setBackgroundResource(R.drawable.lines);
        	    	repeatTextView.performClick();
        	    } 
        	    return true;
        	}
        	});

		scheduleJobActive.setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				 if (scheduleJobActive.isChecked())
					 scheduleJobActive.setChecked(false);
		            else
		            	scheduleJobActive.setChecked(true);	
			}
		});


        scheduleGeoActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scheduleGeoActive.isChecked()) {
                    scheduleGeoActive.setChecked(false);
                    geoSpinner.getSelectedView().setEnabled(false);
                    geoSpinner.setEnabled(false);
                }
                else {
                    scheduleGeoActive.setChecked(true);
                    geoSpinner.getSelectedView().setEnabled(true);
                    geoSpinner.setEnabled(true);
                }
            }
        });

        scheduleGeoActive.setOnTouchListener(new OnTouchListener() {
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    scheduleGeoActive.setBackgroundColor(Color.parseColor("#2C9DC7"));
        	    }
        	    else if (event.getAction() == MotionEvent.ACTION_UP) {
                    scheduleGeoActive.setBackgroundResource(R.drawable.lines);
                    scheduleGeoActive.setCheckMarkDrawable(indicator);


                    if (geoString.isEmpty() && !scheduleGeoActive.isChecked())
                    {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                context);

                        // set title
                        alertDialogBuilder.setTitle("Task Scheduler");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("No geolocation has been found, add now? (Geolocation can be managed from menu)")
                                .setCancelable(false)
                                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, close
                                        // current activity
                                        Intent intent;
                                        intent = new Intent(JobActivity.this, GeoActivity.class);
                                        startActivity(intent);
                                    }
                                })
                                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }

                    else {
                        if (!gpsTracker.canGetLocation() &&
                                !scheduleGeoActive.isChecked())
                        {
                            gpsTracker.showSettingsAlert();
                        }
                        scheduleGeoActive.performClick();
                    }
        	    } 
        	    return true;
        	}
        	});
		
		if (receivedJob !=null)
		{
			loadJob(receivedJob);
			editJob = 1;
		}
		else
        {
            //geoSpinner.getSelectedView().setEnabled(false);
            geoSpinner.setEnabled(false);
        }
	}
	
	
	
	private void loadJob(ScheduledJob j) {
		this.scheduleJobActive.setChecked(j.getEnabled());
		this.repeatTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.repeatTextViewText) + "</b>" +  "<br />" + 
	            "<small>" +j.getStringJobDaysList()  + "</small>"));
		this.timeTextView.setText(Html.fromHtml("<b> <font color='#ffffff'>" + getString(R.string.timeTextViewText) + "</b>" +  "<br />" + 
	            "<small>" + j.getJobHour()+":"+j.getJobMinutes() + "</small>"));
		this.jobLabel.setText(j.getLabel());
		this.jobSpinner.setSelection(Arrays.asList((getResources().getStringArray(R.array.actions))).indexOf(j.getJobActions()));
		selecteddaysList = new ArrayList<String>(j.getJobDaysList());
		hour = Integer.parseInt(j.getJobHour());
		minute = Integer.parseInt(j.getJobMinutes());
        if (geoString.indexOf(j.getGeoLocation()) != -1)
        {
            this.scheduleGeoActive.setChecked(j.getGeoEnabled());
            this.geoSpinner.setSelection(geoString.indexOf(j.getGeoLocation()));

            if (j.getGeoEnabled())
            {
//            geoSpinner.getSelectedView().setEnabled(true);
                geoSpinner.setEnabled(true);
            }
            else
            {
                //          geoSpinner.getSelectedView().setEnabled(false);
                geoSpinner.setEnabled(false);
            }
        }
        else
        {
            receivedJob.setGeoEnabled(false);
            geoSpinner.setEnabled(false);
        }
	}



	public void removeFromList(String str, ArrayList<String> list)
	{
		for (int i=0; i<list.size(); i++) {
			 String val = list.get(i);
			 if (val.equalsIgnoreCase(str)) 
			 {
			   list.remove(i);
			   break;
			 }
			} 
	}

    @Override
    protected void onResume() {
        super.onResume();

        geoadapter.clear();
        geoString = new ArrayList<String>();
        for ( String key : ((PowerScheduler) this.getApplication()).getGeolocations().keySet() ) {
            geoString.add(key);
            geoadapter.insert(key, geoadapter.getCount());
        }
        geoadapter.notifyDataSetChanged();

        if (geoString.isEmpty() && scheduleGeoActive.isChecked())
        {
            scheduleGeoActive.setChecked(false);
            geoSpinner.setEnabled(false);
        }
        else
         if (!geoString.isEmpty() && receivedJob != null)
        {
            if (editJob !=0 && receivedJob.getGeoEnabled()) {
                scheduleGeoActive.setChecked(true);
                geoSpinner.setEnabled(true);
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.actionmain_geolocations:
                Intent intent = new Intent(JobActivity.this, GeoActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
