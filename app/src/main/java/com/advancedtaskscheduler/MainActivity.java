package com.advancedtaskscheduler;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdRequest;


public class MainActivity extends Activity {
	
	private Intent intent;
	private Intent receivedIntent;
	private Button AddJobButton;
	private ArrayList<ScheduledJob> jobs;
	private ListView list;
	private CustomAdapter jobsAdapter;
	private ScheduledJob job;
	private SimpleDateFormat format;
	private SimpleDateFormat formatevery;
	private AlarmManager alarmManager;
    AdView adView;
    AdRequest adRequest;


	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adView = (AdView)this.findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        receivedIntent = getIntent();
        format = new SimpleDateFormat("dd.MM.yyyy 'at' HH:mm");
        formatevery = new SimpleDateFormat("HH:mm");
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        
        jobs = (ArrayList<ScheduledJob>) receivedIntent.getSerializableExtra("myJobs");
        
        if (jobs==null)
        {
        	jobs = readStoredActions();
        	readSettings();
        }
        if (((PowerScheduler) this.getApplication()).getGeolocations() == null)
        {
            readGeolocations();
        }
        
        
        
        
        AddJobButton = (Button)findViewById(R.id.AddJobButton);
        list = (ListView)findViewById(R.id.list);
        jobsAdapter = new CustomAdapter(this, jobs);
        list.setAdapter(jobsAdapter);
        ((PowerScheduler) this.getApplication()).setActivity(this);
        
        
        list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterview, View view, int Viewpos,
					long rowid) {
				job = (ScheduledJob) jobsAdapter.getItem(Viewpos);
				intent = new Intent(MainActivity.this, JobActivity.class);
				intent.putExtra("myJob", job);
				intent.putExtra("myJobs", jobs);
				startActivity(intent);
			}
        
        });
        
        list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				 job = (ScheduledJob) jobsAdapter.getItem(position);
				 startActionMode(modeCallBack);
			     view.setSelected(true);
			     return true;
			
			}
        	
        });
        
        AddJobButton.setOnTouchListener(new OnTouchListener() {
        	@Override
        	public boolean onTouch(View v, MotionEvent event) {
        	    if (event.getAction() == MotionEvent.ACTION_DOWN) {
        	            AddJobButton.setBackgroundColor(Color.parseColor("#2C9DC7"));
        	    }
        	    else if (event.getAction() == MotionEvent.ACTION_UP) {
        	            AddJobButton.setBackgroundColor(Color.TRANSPARENT);
        	            AddJobButton.performClick();
        	    }
        	    return true;
        	}
        	});
        
        AddJobButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			intent = new Intent(MainActivity.this, JobActivity.class);
			intent.putExtra("myJobs", jobs);
			startActivity(intent);
			}
		});
        
    }

    public void readGeolocations()
    {
        FileInputStream fis;
        ObjectInputStream is;
        ArrayList<ScheduledJob> jobs;
        try {
            fis = this.openFileInput(getString(R.string.geoLocationsXMLFile));
            is = new ObjectInputStream(fis);
            ((PowerScheduler) this.getApplication()).setGeolocations((Map<String, ArrayList<Double>>) is.readObject());
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            ((PowerScheduler) this.getApplication()).setGeolocations(new HashMap<String,ArrayList<Double>>());
        }

    }
    
    private ActionMode.Callback modeCallBack = new ActionMode.Callback() {
		
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

			return false;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode mode) {

			
		}
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle("Options");
			mode.getMenuInflater().inflate(R.menu.jobsactions, menu);
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			 
			 int id = item.getItemId();
			 switch (id) {
			   case R.id.delete: {
				 
				 if (job.jobEnabled){
					 
					 if (job.getJobDaysList().size()==0 || job.getJobDaysList().size()==7)
					 {
						 disableAlarm(job);
					 }
					 else
						 disableDaysAlarm(job);
				 }
					 
				 
			     jobsAdapter.removeRecord(job);
			     storeActions();
			     jobsAdapter.notifyDataSetChanged();
			     mode.finish();
			     break;
			          }
			          case R.id.edit: {
			        	    intent = new Intent(MainActivity.this, JobActivity.class);
							intent.putExtra("myJob", job);
							intent.putExtra("myJobs", jobs);
							mode.finish();
							startActivity(intent);
			            break;
			         }
			         default:
			            return false;
			 
			}
			return true;
			
		}
	};
    
    @Override
    public void onStop()
    {
    	storeActions();
    	storeSettings();
    	super.onStop();
    }
    @Override
    public void onRestart()
    {
    	jobs = readStoredActions();
    	jobsAdapter.setJobs(jobs);
    	jobsAdapter.notifyDataSetChanged();
    	super.onRestart();
    }
    
     public void storeActions()
    {
    	FileOutputStream fos;
    	ObjectOutputStream os;
		try {
			fos = this.openFileOutput(getString(R.string.objectXMLFile), Context.MODE_PRIVATE);
			os = new ObjectOutputStream(fos);
	    	os.writeObject(this.jobs);
	    	os.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
    }
    
    public void storeSettings()
    {
    	FileOutputStream fos;
    	ObjectOutputStream os;
		try {
			fos = this.openFileOutput(getString(R.string.settingsXMLFile), Context.MODE_PRIVATE);
			os = new ObjectOutputStream(fos);
	    	os.writeObject(  ((PowerScheduler) this.getApplication()).getSettings() );
	    	os.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
    }
    


    

    
    
    public void readSettings()
    {
    	FileInputStream fis;
    	ObjectInputStream is;
    	
		try {
			fis = this.openFileInput(getString(R.string.settingsXMLFile));
			is = new ObjectInputStream(fis);
			((PowerScheduler) this.getApplication()).setSettings((Map<String, Integer>) is.readObject());  
			is.close();
		} catch (Exception e) {

			e.printStackTrace();
			((PowerScheduler) this.getApplication()).setSettings(new HashMap<String, Integer>());
		}
		
    }
    
    
    public void updateEnabledJob(int jobId){
    	
    	for (int i=0;i<jobs.size();i++){
	    	if (jobs.get(i).id == jobId)
	    	{	
		    	if (jobs.get(i).getEnabled())
		    	{
		    		jobs.get(i).setEnable(false);
		    		if (jobs.get(i).getJobDaysList().size() == 0 || jobs.get(i).getJobDaysList().size() == 7)
		    		disableAlarm(jobs.get(i));
		    		else
		    			disableDaysAlarm(jobs.get(i));
		    	}
		    	else
		    	{
		    		jobs.get(i).setEnable(true);
		    		if (jobs.get(i).getJobDaysList().size() == 0 || jobs.get(i).getJobDaysList().size() == 7)
		    			setAlarm(jobs.get(i));
			    		else
			    			setDaysAlarm(jobs.get(i));
		    	}
		    	
		    	break;
	    	}
    	}
    }
    
    public ArrayList<ScheduledJob> readStoredActions()
    {
    	FileInputStream fis;
    	ObjectInputStream is;
    	ArrayList<ScheduledJob> jobs;
		try {
			fis = this.openFileInput(getString(R.string.objectXMLFile));
			is = new ObjectInputStream(fis);
			jobs = (ArrayList<ScheduledJob>) is.readObject();
			is.close();
		} catch (Exception e) {

			e.printStackTrace();
			jobs = new ArrayList<ScheduledJob>();
		}
    	return jobs;
    }
    
    public void setAlarm(ScheduledJob job)
    {
    	Intent intentAlarm;
    	intentAlarm = new Intent(this, AlarmReceiver.class);
    	intentAlarm.putExtra("myJob", job);
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(this, job.id, intentAlarm, 0);
    	Date date;
    	
    	if (job.getJobDaysList().size() == 0)
    	{
    		Calendar cal = Calendar.getInstance();
    		cal.setTimeInMillis(System.currentTimeMillis());
    		
    		 if (Integer.parseInt(job.getJobHour()) < cal.get(Calendar.HOUR_OF_DAY)) 
    		 {
    			 cal.add(Calendar.DATE, 1);
    		 }
    		 else if (Integer.parseInt(job.getJobHour()) == cal.get(Calendar.HOUR_OF_DAY) && Integer.parseInt(job.getJobMinutes()) <= cal.get(Calendar.MINUTE))	 
    		 {
    			 cal.add(Calendar.DATE, 1);
    		 }
    		
    		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(job.getJobHour()));
            cal.set(Calendar.MINUTE, Integer.parseInt(job.getJobMinutes()));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            }
            else
            {
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            }
    		date = new Date(cal.getTimeInMillis());
    		Toast.makeText(this, "Job scheduled for "+ format.format(date), Toast.LENGTH_LONG).show();

    	}
    	else
    	if (job.getJobDaysList().size() == 7)
    	{
    		Calendar cal = Calendar.getInstance();
    		cal.setTimeInMillis(System.currentTimeMillis());
    		
    		 if (Integer.parseInt(job.getJobHour()) < cal.get(Calendar.HOUR_OF_DAY)) 
    		 {
    			 cal.add(Calendar.DATE, 1);
    		 }
    		 else if (Integer.parseInt(job.getJobHour()) == cal.get(Calendar.HOUR_OF_DAY) && Integer.parseInt(job.getJobMinutes()) <= cal.get(Calendar.MINUTE))	 
    		 {
    			 cal.add(Calendar.DATE, 1);
    		 }
    		
    		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(job.getJobHour()));
            cal.set(Calendar.MINUTE, Integer.parseInt(job.getJobMinutes()));
    		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    		date = new Date(cal.getTimeInMillis());
    		Toast.makeText(this, "Job scheduled every day at " + formatevery.format(date), Toast.LENGTH_LONG).show();
    	}	
    }
    
    public int convertDays(String day)
    {
    	
    	if (day.equals("Monday"))
    		return Calendar.MONDAY;
    	else if (day.equals("Tuesday"))
    		return Calendar.TUESDAY;
    	else if (day.equals("Wednesday"))
    		return Calendar.WEDNESDAY;
    	else if (day.equals("Thursday"))
    		return Calendar.THURSDAY;
    	else if (day.equals("Friday"))
    		return Calendar.FRIDAY;
    	else if (day.equals("Saturday"))
    		return Calendar.SATURDAY;
    	else 
    		return Calendar.SUNDAY;
    }
    
    
    public void setDaysAlarm(ScheduledJob job)
	{
    	Intent intentAlarm;
    	intentAlarm = new Intent(this, AlarmReceiver.class);
    	intentAlarm.putExtra("myJob", job);
    	PendingIntent pendingIntent;
    	Date date= new Date();
    	String toastString="";
    	
    	
    	for (int i=0;i<job.getJobDaysList().size();i++)
    	{
    		pendingIntent= PendingIntent.getBroadcast(this, job.id * 10+i, intentAlarm, 0);
    		Calendar cal = Calendar.getInstance();
    		cal.setTimeInMillis(System.currentTimeMillis());
    	 	
    		int day = cal.get(Calendar.DAY_OF_WEEK);
    		
    		if (Integer.parseInt(job.getJobHour()) < cal.get(Calendar.HOUR_OF_DAY) && convertDays(job.getJobDaysList().get(i)) == day) 
   		 	{
   			 cal.add(Calendar.DATE, 7);
   		 	}
   		 else if (Integer.parseInt(job.getJobHour()) == cal.get(Calendar.HOUR_OF_DAY) && Integer.parseInt(job.getJobMinutes()) <= cal.get(Calendar.MINUTE) && convertDays(job.getJobDaysList().get(i)) == day)	 
   		 	{
   			 cal.add(Calendar.DATE, 7);
   		 	}
   		 else
   			cal.add(Calendar.DATE, (Calendar.SATURDAY - day + convertDays(job.getJobDaysList().get(i))) % 7);
   			//cal.add(Calendar.DATE, Math.abs(day-convertDays(job.getJobDaysList().get(i))));
    		
    		
    		
    		
    		cal.set(Calendar.DAY_OF_WEEK, convertDays(job.getJobDaysList().get(i)));
    		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(job.getJobHour()));
            cal.set(Calendar.MINUTE, Integer.parseInt(job.getJobMinutes()));            
    		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), AlarmManager.INTERVAL_DAY*7, pendingIntent);
    		date = new Date(cal.getTimeInMillis());
    		toastString=toastString+job.getJobDaysList().get(i)+ ", ";
    	}
    	
    	Toast.makeText(this, "Job scheduled "+ toastString.substring(0, toastString.length()-2) +  " at " + formatevery.format(date), Toast.LENGTH_LONG).show();
	}
    
    public void disableDaysAlarm(ScheduledJob job)
    {
    	Intent intentAlarm;
    	intentAlarm = new Intent(this, AlarmReceiver.class);
    	intentAlarm.putExtra("myJob", job);
    	PendingIntent pendingIntent;
    	
    	for (int i=0;i<job.getJobDaysList().size();i++)
    	{
    	    pendingIntent = PendingIntent.getBroadcast(this, job.id*10+i, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
        	alarmManager.cancel(pendingIntent);
    	}
    
    }
    
    
    public void disableAlarm(ScheduledJob job)
    {
    	Intent intentAlarm;
    	intentAlarm = new Intent(this, AlarmReceiver.class);
    	intentAlarm.putExtra("myJob", job);
    	PendingIntent pendingIntent = PendingIntent.getBroadcast(this, job.id, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
    	//PendingIntent pendingIntent = ((PowerScheduler) this.getApplication()).getAlarmIntent(job.id);
    	alarmManager.cancel(pendingIntent);
    	
    }
    
    
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.actionmain_geolocations:
                intent = new Intent(MainActivity.this, GeoActivity.class);
                startActivity(intent);
                return true;
            case R.id.actionmain_settings:
                intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
}
