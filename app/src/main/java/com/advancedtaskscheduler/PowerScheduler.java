package com.advancedtaskscheduler;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

public class PowerScheduler  extends Application{
	
	private Map<String, Integer> settings;
	private Activity MainActivity;
	private Map<String, LatLng> geolocations;
    private Map<String, ArrayList<Double>> serializableGeolocations;
	
	private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                activity.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
        mContext = this;
    }
    public static Context getContext(){
        return mContext;
    }
	public Integer getSettingValue(String setting){
		return this.settings.get(setting);		
	}
	public void setSettingValue(String setting, Integer value){
		this.settings.put(setting, value);
	}
	public Map<String, Integer> getSettings(){
		return this.settings;
	}
	public void setSettings(Map<String, Integer> setting){
		this.settings = setting;
	}
	public Activity getMainActivity(){
		return this.MainActivity;
	}
	public void setActivity(Activity mainAct){
		this.MainActivity=mainAct;
	}
    public Map<String, LatLng> getGeolocations(){
        return this.geolocations;
    }
    public Map<String, ArrayList<Double>> getSerializableGeolocations() {return  this.serializableGeolocations;}
    public void setGeolocations(Map<String, ArrayList<Double>> geolocations){
        this.serializableGeolocations=geolocations;
        if (serializableGeolocations.isEmpty())
        {
            this.geolocations = new HashMap<String, LatLng>();
        }
        else
        {
            this.geolocations = new HashMap<String, LatLng>();
            for(Map.Entry<String, ArrayList<Double>> entry: this.serializableGeolocations.entrySet()) {
                this.geolocations.put(entry.getKey(), new LatLng(entry.getValue().get(0), entry.getValue().get(1)));
            }

        }
    }
    public void setGeolocation(String name, LatLng loc){
        ArrayList<Double> conv = new ArrayList<Double>();
        conv.add(loc.latitude);
        conv.add(loc.longitude);
        this.geolocations.put(name, loc); this.serializableGeolocations.put(name, conv);
    }
    public void removeGeolocation (String name){this.getGeolocations().remove(name); this.serializableGeolocations.remove(name);}
    public LatLng getGeolocation(String name) {
        if (this.geolocations == null)
        {
          readGeolocations();
        }
        return this.geolocations.get(name);
    }
    public void readGeolocations()
    {
        FileInputStream fis;
        ObjectInputStream is;
        try {
            fis = this.openFileInput(getString(R.string.geoLocationsXMLFile));
            is = new ObjectInputStream(fis);
            setGeolocations((Map<String, ArrayList<Double>>) is.readObject());
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
            setGeolocations(new HashMap<String,ArrayList<Double>>());
        }

    }

}
